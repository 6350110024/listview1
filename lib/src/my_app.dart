import 'package:flutter/material.dart';
import 'package:listview/src/pags/Example2.dart';
import 'package:listview/src/pags/Example3.dart';
import 'package:listview/src/pags/Example4.dart';


import 'pags/Example1.dart';

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'listview Demo',
      theme: ThemeData(
        primarySwatch: Colors.blue,
        visualDensity: VisualDensity.adaptivePlatformDensity
      ),
      home: Example3(),
    );
  }
}